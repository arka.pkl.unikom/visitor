<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Visit System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/assetsUser/images/icon/favicon.ico">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/metisMenu.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/typography.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/default-css.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/assetsUser/css/responsive.css">
    <!-- modernizr css -->
    <script src="<?php echo base_url();?>assets/assetsUser/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="index.html"><img src="<?php echo base_url();?>assets/assetsUser/images/icon/logo.png" alt="logo"></a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                    	<div class="user-profile">
                                <img class="avatar user-thumb" src="<?php echo base_url();?>assets/assetsUser/images/author/avatar.png" alt="avatar">
                            <h4 class="user-name dropdown-toggle" data-toggle="dropdown"><?php echo $this->session->userdata('name');?> </h4>
                            </div>
						<ul class="metismenu" id="menu">
                            <li>
                                <a href="<?php echo site_url('c_user/c_user/index'); ?>" aria-expanded="true"><i class="fa fa-table"></i><span>Visitor Log</span></a>
                            </li>
                            <li class="active"><a href="javascript:void(0)"><i class="ti-receipt"></i> <span>Report</span></a></li>
                           <li><a href="<?php echo site_url('login/logout');?>"><i class="ti-power-off"></i> <span>Log Out</span></a></li>
                            
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->
        <!-- main content area start -->
        <div class="main-content">
            <!-- header area start -->
            <div class="header-area">
                <div class="row align-items-center">
                    <!-- nav and search button -->
                    <div class="col-md-6 col-sm-8 clearfix">
                        <div class="nav-btn pull-left">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="search-box pull-left">
                            <form action="#">
                                <input type="text" name="search" placeholder="Search..." required>
                                <i class="ti-search"></i>
                            </form>
                        </div>
                    </div>
                    <!-- profile info & task notification -->
                    <div class="col-md-6 col-sm-4 clearfix">
                        <ul class="notification-area pull-right">
                            <li id="full-view"><i class="ti-fullscreen"></i></li>
                            <li id="full-view-exit"><i class="ti-zoom-out"></i></li>
                            </ul>         
                </div>
            </div>

            <!-- header area end -->
            <!-- page title area start -->
            <br>
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Visitor Log</h4>
                        </div>
                    </div>
                  </div>  
            </div>                  
                
            </div>

            <!-- page title area end -->
            <!-- Progress Table start -->
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">Report Visitor</h4>
                                <div class="single-table">
                                    <div class="table-responsive">
                                        <table class="table table-hover progress-table text-center">
                                            <thead class="text-uppercase">
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Company</th>
                                                    <th scope="col">Addres</th>
                                                    <th scope="col">Phone/Mobile</th>
                                                    <th scope="col">Email</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Time Cek In</th>
                                                    <th scope="col">Time Cek Out</th>
                                                    <th scope="col">Note</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $count = 0;
                                                    foreach ($visit->result() as $row) :
                                                 ?>
                                                <tr>
                                                    <th scope="row"><?php echo $row->id_visit;?></th>
                                                    <td><?php echo $row->visitor_name;?></td>
                                                    <td><?php echo $row->id_company;?></td>
                                                    <td><?php echo $row->visitor_addres;?></td>
                                                    <td><?php echo $row->visitor_phone;?></td>
                                                    <td><?php echo $row->email;?></td>
                                                    <td><?php echo $row->date;?></td>
                                                    <td><?php echo $row->time_check_in;?></td>
                                                    <td><?php echo $row->time_check_out;?></td>
                                                    <td><?php echo $row->notes;?></td>
                                                </tr>
                                                
                    <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Progress Table end -->
                <!-- row area end -->
                
                    <!-- exchange area end -->
               </div>
            </div>
        </div>
        <!-- main content area end -->
        <!-- footer area start-->
        <footer>
            <div class="footer-area">
                <p>© Copyright 2018. All right reserved. Template by <a href="https://colorlib.com/wp/">Colorlib</a>.</p>
            </div>
        </footer>
        <!-- footer area end-->
    
    <!-- offset area end -->
    <!-- jquery latest version -->
    <script src="<?php echo base_url();?>assets/assetsUser/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="<?php echo base_url();?>assets/assetsUser/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/assetsUser/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/assetsUser/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/assetsUser/js/metisMenu.min.js"></script>
    <script src="<?php echo base_url();?>assets/assetsUser/js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/assetsUser/js/jquery.slicknav.min.js"></script>

    <!-- start chart js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <!-- start highcharts js -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <!-- start zingchart js -->
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
    <script>
    zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
    ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
    </script>
    <!-- all line chart activation -->
    <script src="<?php echo base_url();?>assets/assetsUser/js/line-chart.js"></script>
    <!-- all pie chart -->
    <script src="<?php echo base_url();?>assets/assetsUser/js/pie-chart.js"></script>
    <!-- others plugins -->
    <script src="<?php echo base_url();?>assets/assetsUser/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/assetsUser/js/scripts.js"></script>
</body>

</html>
