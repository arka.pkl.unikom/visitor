<?php
class C_user extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model('visit_model');
  }
  function index(){
    $data['visit'] = $this->visit_model->get_visit();
    $this->load->view('user/index',$data);
  }

  function report(){
    $data['visit'] = $this->visit_model->get_visit();
    $this->load->view('user/report',$data);
  }
}